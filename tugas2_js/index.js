const playGame = () => {
  let ronde = 1;
  alert(
    "Selamat datang di game tebak angka\n" +
      "kamu diminta memnebak angka 1-3\n" +
      "dan kamu akan bermain 5 ronde." +
      "player yang behasil mengumpulkan tebakan terbanyak akan menang\n" +
      "SELAMAT BERMAIN!!"
  );

  let skorP1 = 0;
  let skorP2 = 0;
  while (ronde <= 5) {
    let p1 = parseInt(prompt("Player1:masukan angka"));
    let p2 = parseInt(prompt("Player2:masukan angka"));

    const angka = Math.ceil(Math.random() * 3);
    ronde++;
    console.log(ronde);

    if (!p1 || !p2) {
      alert("salah satu player belum menebak");
      const restart = confirm("ulangi?");
      restart == true ? ronde-- : (ronde += 100);
    } else if (p1 == p2) {
      alert("tebakan tidak boleh sama");
      const restart = confirm("ulangi?");
      restart == true ? ronde-- : (ronde += 100);
    } else if (p1 > 3 || p2 > 3) {
      alert("tebakan tidak boleh lebih dari 3");
      const restart = confirm("ulangi?");
      restart == true ? ronde-- : (ronde += 100);
    } else if (p1 == angka) {
      skorP1++;
      alert("Player 1 win");
      alert(
        `Nilai yang dicari ${angka}\n` +
          "-----------\n" +
          `-Player 1 :${skorP1}\n` +
          `-Player 2 :${skorP2}\n`
      );
      if (ronde <= 5) {
        const babak = confirm(`ronde ${ronde}`);
        if (!babak) {
          ronde += 100;
        }
      }
    } else if (p2 == angka) {
      skorP2++;
      alert("Player 2 win");
      alert(
        `Nilai yang dicari ${angka}\n` +
          "-----------\n" +
          `-Player 1 :${skorP1}\n` +
          `-Player 2 :${skorP2}\n`
      );
      if (ronde <= 5) {
        const babak = confirm(`ronde ${ronde}`);
        if (!babak) {
          ronde += 100;
        }
      }
    } else if (p1 != angka && p2 != angka) {
      alert("tidak ada yang sama hasil seri");
      alert(
        `Nilai yang dicari ${angka}\n` +
          "-----------\n" +
          `-Player 1 :${skorP1}\n` +
          `-Player 2 :${skorP2}\n`
      );
      if (ronde <= 5) {
        const babak = confirm(`ronde ${ronde}`);
        if (!babak) {
          ronde += 100;
        }
      }
    }
    if (ronde > 5 && ronde < 10) {
      if (skorP1 > skorP2) {
        alert("Goodjob Player 1");
      } else if (skorP1 < skorP2) {
        alert("Goodjob Player 2");
      } else {
        alert("goodjob player");
      }
    }
  }
};

playGame();
