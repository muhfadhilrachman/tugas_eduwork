const listUmurSiswa = [12, 12, 13, 14, 15, 16, 17];

const siswaKelas10 = listUmurSiswa[4];
const siswaKelas11 = listUmurSiswa[5];
const siswaKelas12 = listUmurSiswa[6];
const listUmurSiswaSma = [siswaKelas10, siswaKelas11, siswaKelas12];

const ketuaKelas = {
  nama: "ari",
  kelas: 12,
  umur: siswaKelas12,
  pengalamanOsis: {
    namaKegiatan: "mpls",
    waktu: "24jam",
  },
};

const {
  nama,
  kelas,
  umur,
  pengalamanOsis: { namaKegiatan },
} = ketuaKelas;
console.log(nama);
function biodata(profile) {
  console.log(
    `perkenalkan namaku ${profile.nama}, aku siswa kelas ${profile.kelas}, umurku ${profile.umur} tahun dan aku punya pengalaman menjadi ketua kegiatan di osis yakni kegiatan ${profile.pengalamanOsis.namaKegiatan}`
  );
}

biodata(ketuaKelas);
