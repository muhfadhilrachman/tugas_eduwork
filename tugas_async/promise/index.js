let data;

const getData = (search) => {
  let api =
    search != undefined
      ? `https://newsapi.org/v2/top-headlines?q=${search}&country=id&apiKey=75eaf43dc50f467fac0a8aed757ad2d5`
      : `https://newsapi.org/v2/top-headlines?country=id&apiKey=75eaf43dc50f467fac0a8aed757ad2d5`;
  data = fetch(api);
  data
    .then((res) => {
      return res.json();
    })
    .finally(() => {
      console.log("berhasil");
      return (document.getElementById(
        "cont"
      ).innerHTML = `<div class="spinner-border text-primary" role="status">
      <span class="visually-hidden">Loading...</span>
    </div>`);
    })
    .then((val) => {
      let content = val?.articles?.map((res) => {
        return `<div class='col-4 mt-3'>
        <div class='card' >
        <img src="${res.urlToImage}" class="card-img-top" alt="...">
                          <div class='card-body'>
                          <h5 className="card-title fw-bold"$>${res.title}</h5>
                          <div class='d-flex justify-content-between align-items-center'>
                          <small class='card-subtitle mb-2 text-muted'>${res.author}</small>
                          <small class='card-subtitle mb-2 text-muted'>${res.publishedAt}</small>
                          </div>
                          <p className="car-text">${res.content}</p>
    
                            <button class='btn btn-primary'>Read More...</button>
                          </div>
                         </div>
        </div>`;
      });
      const empty = `
      <div class='col'>
      <div class="alert  alert-danger" role="alert">
      keyword tidak ditemukan
      </div>
      </div>
      `;
      return (document.getElementById("cont").innerHTML =
        val.articles.length == 0 ? empty : content.join(""));
    })
    .catch((err) => console.log(err));
};

getData();

function handleChange() {
  var val = document.getElementById("search").value;
  console.log(val);
  getData(val);
}
