const murid = [
  {
    nama: "nisa",
    umur: 13,
    orangtua: {
      ibu: "dewi",
      ayah: "arif",
    },
  },
  {
    nama: "dila",
    umur: 12,
    orangtua: {
      ibu: "ajeng",
    },
  },
  {
    nama: "agus",
    umur: 12,
    orangtua: {
      ibu: "ai",
    },
  },
  {
    nama: "nerisa",
    umur: 14,
    orangtua: {
      ibu: "nyai",
      ayah: "ujang",
    },
  },
];

const umurMurid = murid.map((val) => val.umur);
const muridLayakDapatSantunan = murid.filter((val) => !val.orangtua.ayah);
const jumlahMurid = murid.length;
